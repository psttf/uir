package lib

trait Program extends ProgramAlgebra {
  def impl: TFunctionImpl

  def program(argNames: List[String]): TProgram =
    addFunction(
      emptyProgram("program"),
      functionDecl("declaration", argNames),
      impl
    )
}
