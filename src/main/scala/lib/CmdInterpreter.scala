package lib

import cats.Monad

trait CmdInterpreter extends ProgramAlgebra {
  type TProgram = Array[String] => Option[List[(String, String)]]
  type TFunctionDecl = List[(String, String)] => Option[List[(String, String)]]
  type TFunctionImpl = List[(String, String)] => List[(String, String)]

  def emptyProgram(name: String): TProgram = _ => None

  def addFunction(
                   program: TProgram,
                   decl: TFunctionDecl,
                   impl: TFunctionImpl
                 ): TProgram =
    args => {
      val parsedArgs = parseArgs(args)
      program(args).orElse(decl(parsedArgs).map(_ => impl(parsedArgs)))
    }

  def functionDecl(name: String, argNames: List[String]): TFunctionDecl =
    args => {
      if (argNames.forall(arg => args.exists(_._1 == arg))) Some(args)
      else None
    }

  def transform[T](input: Option[List[(String, String)]], keys: List[String], f: List[String] => T): Option[T]

  private def parseArgs(args: Array[String]): List[(String, String)] = {
    args.map { arg =>
      val parts = arg.split("=", 2)
      parts(0).stripPrefix("--") -> parts(1)
    }.toList
  }
}

object ProgramCmd extends Program with CmdInterpreter {
  override def impl: List[(String, String)] => List[(String, String)] = args => {
    args
  }

  override def transform[T](input: Option[List[(String, String)]], keys: List[String], f: List[String] => T): Option[T] = {
    input.flatMap { list =>
      if (keys.forall(key => list.exists(_._1 == key))) {
        val values = keys.flatMap(key => list.find(_._1 == key).map(_._2))
        if (values.length == keys.length) Some(f(values)) else None
      } else {
        None
      }
    }
  }
}