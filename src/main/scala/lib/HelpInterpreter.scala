package lib

trait HelpInterpreter extends ProgramAlgebra {
  type TProgram = String
  type TFunctionDecl = String
  type TFunctionImpl = Unit

  def emptyProgram(name: String): String = s"\n\nProgram $name usage:"

  def addFunction(program: String, decl: String, impl: Unit): String =
    program + s"\n  <program> $decl"

  def functionDecl(name: String, argNames: List[String]): String =
    s"$name " + argNames.map(argName => s"--$argName <value>").mkString(" ")
}

object ProgramHelp extends Program with HelpInterpreter {
  val impl: Unit = ()
}
