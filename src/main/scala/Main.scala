import domain.User
import lib.{ProgramCmd, ProgramHelp}

object Main {
  def main(args: Array[String]): Unit = {
    val argNames = List("name", "surname", "age")

    println(ProgramHelp.program(argNames)+"\n")

    val testArgs = args // testArgs = Array("--name=Иван", "--surname=Иванов", "--age=12")

    val result =  ProgramCmd.program(argNames)(testArgs)

    val user: Option[User] = ProgramCmd.transform(result, argNames, {
      case List(name, surname, age) => User(name, surname, age)
    })

    println(user)
  }
}